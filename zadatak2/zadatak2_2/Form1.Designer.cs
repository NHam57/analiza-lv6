﻿namespace zadatak2_2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.rijec = new System.Windows.Forms.Label();
            this.textBox_slovo = new System.Windows.Forms.TextBox();
            this.textBox_rijec = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.Try_letter = new System.Windows.Forms.Button();
            this.try_word = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.broj_preostalih = new System.Windows.Forms.Label();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button1.Location = new System.Drawing.Point(28, 22);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(141, 41);
            this.button1.TabIndex = 0;
            this.button1.Text = "Nova riječ";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // rijec
            // 
            this.rijec.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.rijec.Location = new System.Drawing.Point(12, 203);
            this.rijec.Name = "rijec";
            this.rijec.Size = new System.Drawing.Size(669, 70);
            this.rijec.TabIndex = 1;
            this.rijec.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.rijec.Click += new System.EventHandler(this.label1_Click);
            // 
            // textBox_slovo
            // 
            this.textBox_slovo.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBox_slovo.Location = new System.Drawing.Point(392, 28);
            this.textBox_slovo.Name = "textBox_slovo";
            this.textBox_slovo.Size = new System.Drawing.Size(33, 32);
            this.textBox_slovo.TabIndex = 2;
            this.textBox_slovo.TextChanged += new System.EventHandler(this.textBox_slovo_TextChanged);
            // 
            // textBox_rijec
            // 
            this.textBox_rijec.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBox_rijec.Location = new System.Drawing.Point(392, 122);
            this.textBox_rijec.Name = "textBox_rijec";
            this.textBox_rijec.Size = new System.Drawing.Size(145, 32);
            this.textBox_rijec.TabIndex = 3;
            this.textBox_rijec.TextChanged += new System.EventHandler(this.textBox_rijec_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(193, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(179, 26);
            this.label1.TabIndex = 4;
            this.label1.Text = "Pogađanje slova:";
            this.label1.Click += new System.EventHandler(this.label1_Click_1);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(193, 125);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(173, 26);
            this.label2.TabIndex = 5;
            this.label2.Text = "Pogađanje riječi:";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // Try_letter
            // 
            this.Try_letter.Location = new System.Drawing.Point(454, 31);
            this.Try_letter.Name = "Try_letter";
            this.Try_letter.Size = new System.Drawing.Size(125, 29);
            this.Try_letter.TabIndex = 6;
            this.Try_letter.Text = "Pokušaj slovo";
            this.Try_letter.UseVisualStyleBackColor = true;
            this.Try_letter.Click += new System.EventHandler(this.button2_Click);
            // 
            // try_word
            // 
            this.try_word.Location = new System.Drawing.Point(570, 127);
            this.try_word.Name = "try_word";
            this.try_word.Size = new System.Drawing.Size(111, 29);
            this.try_word.TabIndex = 7;
            this.try_word.Text = "Pokušaj riječ";
            this.try_word.UseVisualStyleBackColor = true;
            this.try_word.Click += new System.EventHandler(this.try_word_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 85);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(160, 17);
            this.label3.TabIndex = 8;
            this.label3.Text = "Broj preostalih pokušaja";
            // 
            // broj_preostalih
            // 
            this.broj_preostalih.AutoSize = true;
            this.broj_preostalih.Font = new System.Drawing.Font("Microsoft Sans Serif", 22F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.broj_preostalih.Location = new System.Drawing.Point(40, 112);
            this.broj_preostalih.Name = "broj_preostalih";
            this.broj_preostalih.Size = new System.Drawing.Size(118, 42);
            this.broj_preostalih.TabIndex = 9;
            this.broj_preostalih.Text = "label4";
            this.broj_preostalih.Click += new System.EventHandler(this.broj_preostalih_Click);
            // 
            // folderBrowserDialog1
            // 
            this.folderBrowserDialog1.HelpRequest += new System.EventHandler(this.folderBrowserDialog1_HelpRequest);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(693, 282);
            this.Controls.Add(this.broj_preostalih);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.try_word);
            this.Controls.Add(this.Try_letter);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox_rijec);
            this.Controls.Add(this.textBox_slovo);
            this.Controls.Add(this.rijec);
            this.Controls.Add(this.button1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label rijec;
        private System.Windows.Forms.TextBox textBox_slovo;
        private System.Windows.Forms.TextBox textBox_rijec;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button Try_letter;
        private System.Windows.Forms.Button try_word;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label broj_preostalih;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
    }
}